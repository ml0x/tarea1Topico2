
from clases import Animal
from clases import Perro

lista_objeto = []

# Trae un array de listas #
def traer_objetos ():
    lista = []
    lista_objeto_aux = []
    f = open('lista_animales.txt', 'r')
    archivo = f.read()
    lista_aux = []
    separado = archivo.split("*")
    for string in separado:
        linea = string.split(",")
        arraAux = []
        for string2 in linea:
            arraAux.append(string2)
        lista_aux.append(arraAux)
    lista = lista_aux
    lista.pop()
    for linea in lista:
        obj = Perro(linea[0], linea[1])
        obj.set_energia(int(linea[2]))
        lista_objeto_aux.append(obj)
    f.close()
    lista_objeto = lista_objeto_aux
    return lista_objeto_aux

def actualizar_txt (ListaObjeto):
    f = open('lista_animales.txt', 'w')
    for objeto in lista_objeto:
        nombre = objeto.get_nombre()
        edad = str(objeto.get_edad())
        energia = str(objeto.get_energia())
        tipo = objeto.get_tipo()
        linea = nombre + ',' +edad + ',' +energia + ',' + tipo + '*'
        f.write(linea)
    f.close()

def aparear(cadena):
    lista_objeto = traer_objetos()
    cadena_separada = cadena.split("+")

    if(("+" in cadena) and (cadena.count("+") == 1)):
        objeto1 = ""
        objeto2 = ""
        nombre = ""
        edad = 0
        energia = 0
        val_aux1 = 0
        val_aux2 = 0
        for objeto in lista_objeto:
            if objeto.get_nombre() == cadena_separada[0]:
                val_aux1 = val_aux1 + 1
                objeto1 = objeto

        for objeto in lista_objeto:
            if objeto.get_nombre() == cadena_separada[1]:
                val_aux2 = val_aux2 + 1
                objeto2 = objeto

        if(val_aux1 == 0 or val_aux1 == 0):
            print("Uno o ambos animales no existen")
        else:
            nombre = objeto1.get_nombre() + objeto2.get_nombre()
            energia = int(objeto1.get_energia()) + int(objeto2.get_energia())
            if(int(objeto1.get_edad()) < int(objeto2.get_edad()) or int(objeto1.get_edad()) == int(objeto2.get_edad())):
                edad = int(objeto1.get_edad())
            else:
                edad = int(objeto2.get_edad())
            
            nuevo_perro = Perro(nombre, edad)
            nuevo_perro.set_energia(energia)

            valAux3 = 0
            for objeto in lista_objeto:
                if objeto.get_nombre() == nuevo_perro.get_nombre():
                    valAux3 = valAux3 + 1
            if valAux3 > 0:
                print("Error al crear nuevo animal debido a que ya existe")
            else:
                lista_objeto.append(nuevo_perro)
                f = open('lista_animales.txt', 'a')
                string = nuevo_perro.get_nombre()+','+str(nuevo_perro.get_edad())+',' + \
                    str(nuevo_perro.get_energia())+','+nuevo_perro.get_tipo()+'*'
                f.write(string)
                print("Nacio un nuevo animal con las caracteristicas: ", nuevo_perro.get_descripcion())
                f.close()
    else:
        print("formato incorrecto")


val_menu = False
print("\n")
print("1.- Crear Animal")
print("2.- Elegir Animal")
print("3.- Aparear animales")
print("4.- Ver animales disponibles")

try:
    option = int(input("Ingresa una opcion... "))
except:
     print ("\nError al selecionar opcion...")
     val_menu = True
    
if val_menu != True:
    if(option):
        if option == 1:
            nombre = input("\nIngresa un nombre: ")
            edad = int(input("Ingresa Edad: "))
            obj = Perro(nombre, edad)
            print("\nHaz creado al animal: ", obj.get_descripcion())
            f = open('lista_animales.txt', 'a')
            string = obj.get_nombre()+','+str(obj.get_edad())+',' + \
                str(obj.get_energia())+','+obj.get_tipo()+'*'
            f.write(string)
            f.close()

        elif option == 2:
            lista_objeto = traer_objetos()
            elementoLista = input("\nIngrese el nombre del animal a seleccionar: ")
            contador = 0
            for objeto in lista_objeto:
                objeto.get_energia()
                if objeto.get_nombre() == elementoLista:
                    contador = contador + 1
                    print(objeto.get_descripcion())
                    print("\n")
                    print("1.- Caminar")
                    print("2.- Comer")
                    print("3.- Enfermarse")
                    print("4.- Ladrar")
                    print("5.- Correr")
                    opcionAccion = int(input("Seleccione una accion: "))

                    if opcionAccion == 1:
                        objeto.caminar()
                    if opcionAccion == 2:
                        objeto.comer()
                    if opcionAccion == 3:
                        objeto.enfermarse()
                    if opcionAccion == 4:
                        objeto.ladrar()
                    if opcionAccion == 5:
                        objeto.correr()
                    actualizar_txt(lista_objeto)

            if contador == 0:
                 print("\nNo se ha encontrado el animal...")

        elif option == 3:
            animales_consulta = input("\nIngrese el nombre del animales separados por un signo + 'Ej: animal1+animal2': ")
            aparear(animales_consulta)

        elif option == 4:
            f = open('lista_animales.txt', 'r')
            archivo = f.read()
            archivo = archivo.replace(",", " ")
            archivo = archivo.replace("*", "\n")
            print("\n"+archivo)
            f.close()
        else:
            print("\nDebes ingresar solo numeros")
else:
    print("Cerrando programa ...")
