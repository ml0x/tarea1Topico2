import random

class Animal():
    def __init__(self, nombre, edad, energia):
        self.__nombre = nombre
        self.__edad = edad
        self.__energia = energia

    def get_nombre(self):
        return self.__nombre

    def get_edad(self):
        return self.__edad

    def get_energia(self):
        return self.__energia

    def set_energia(self, x):
        self.__energia = x

    def get_descripcion(self):
        return self.get_nombre(), self.get_edad(), self.get_energia(), self.get_tipo()

    def caminar(self):
        if self.__energia <= 0:
            print('\nEl perro no tiene energia para realizar esta accion ')
        else:    
            restar_caminar = self.__energia - 200
            self.set_energia(restar_caminar)
            print("\nCaminando ...")
            print("\nLa nueva energia es de: " + str(self.__energia))

    def comer(self):
        sumar_energia = self.__energia + 300
        self.set_energia(sumar_energia)
        print("\nNom nom nom ...")
        print("\nLa nueva energia es de: " + str(self.__energia))

    def enfermarse(self):
        total = self.__energia
        descuento_enfermarse = self.__energia - int((total*(random.randint(0, 100)))/100) # Calculo de daño en porcentaje aleatoreo no mayor al 100% #
        self.set_energia(descuento_enfermarse)
        print("\nCoof Coof ...")
        print("\nLa nueva energia es de: " + str(self.__energia))


class Perro(Animal):

    def __init__(self, nombre, edad):
        super().__init__(nombre, edad, 1000)
        self.__tipo = "perro"

    def get_tipo(self):
        return self.__tipo

    def ladrar(self):
        if self.get_energia()<= 0:
            print('\nEl perro no tiene energia para realizar esta accion ')
        else:
            num = self.get_energia() - 100
            self.set_energia(num)
            print("\nGuau! Guau ...")
            print("\nLa nueva energia es de: " + str(self.get_energia()))

    def correr(self):
        if self.get_energia()<= 0:
            print('\nEl perro no tiene energia para realizar esta accion ')
        else:
            num = self.get_energia() - 400
            self.set_energia(num)
            print("\nCorriendo ...")
            print("\nLa nueva energia es de: " + str(self.get_energia()))